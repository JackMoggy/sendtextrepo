$(document).ready(function() {
  $('form.sendText').on('submit', function() {
    var formData = {
      'textMessage': $('textarea[name=textMessage]').val(),
      'mobileNumber': $('input[name=mobileNumber]').val(),
    };

    // process the form
    $.ajax({
      type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
      url         : '/sendtext', // the url where we want to POST
      data        : formData, // our data object
      dataType    : 'json', // what type of data do we expect back from the server
      encode      : true
    })
    // using the done promise callback
    .done(function(data) {

      // Everything went well!
      // Clear form? Say success
      $('.my-notify-success').show();
      $('form').slideUp()
    })

    .error(function(err) {
      console.log(err);
      $('.my-notify-failure').slideDown();
      $('.my-notify-failure').show();
      $('.my-notify-failure .message').html('Error: ' + err.responseJSON.error);
      // Something went wrong? Display error?
    });

    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();

  });
});