var express = require('express');
var router = express.Router();
var accountSid = 'AC7cc5fd11587fd8a8587e04db015bc0ab';
var authToken = '9432761b44ba22710eaf9095ec377d05';
var twilio = require('twilio')(accountSid, authToken);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var validNumber = function(number) {
  return /^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/.test(number);
};

/* POST text end-point. */
router.post('/sendtext', function (req, res) {
  // console.log(req.body);
  // You're gonna want to check mobileNumnber & textMessage exist first!
  var number = req.body.mobileNumber;

  if(!validNumber(number)) {
    return res.status(500).json({ success: false, error: 'Invalid number' });
  }

  twilio.messages.create({
    body: req.body.textMessage,
    to: number,
    from: "+441808272012"
  }, function(err, message) {
    if (err) {
      console.log(err, message);
      var error = err.message;
      switch(err.code) {
        case 21608:
          error = 'Number not verified.';
          break;
        case 21617:
          error = 'Message exceeds 1600 character limit.';
          break;
      };
      return res.status(500).json({ success: false, error: error });
    }
    console.log(message.sid);


    // So, wait for the message to be sent first, then render!
    // res.render('index', { title: 'Express' });
    // Actually...I wouldn't render ...I'd redirect back to the homepage...
    res.json({ success: true });

  });
});

module.exports = router;